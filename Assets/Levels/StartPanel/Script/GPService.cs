﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class GPService : MonoBehaviour
{
    public Slider loadIndicator;
    // Start is called before the first frame update
    void Start()
    {
        startGPService();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startGPService()
    {
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate(success =>
        {
            if (success)
            {

            }
            else
            {

            }
        });
    }

    public void btnStartPlay()
    {
        loadIndicator.gameObject.SetActive(true);
        SceneManager.LoadSceneAsync(1);
    }
}
