﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasController : MonoBehaviour
{
    public Canvas mainMenu;
    public Canvas settingMenu;
    public Canvas choicePlayerMenu;
    public Canvas statisticMenu;
    public Canvas progressMenu;
    public Canvas ShopMenu;
    public Canvas ShopMenu_Fon;
    public Canvas ShopMenu_Sound;
    public Canvas ShopMenu_Inteface;
    public Canvas PlayGame;

    void Start()
    {
        mainMenu.GetComponent<Canvas>().enabled = true;
        settingMenu.GetComponent<Canvas>().enabled = false;
        choicePlayerMenu.GetComponent<Canvas>().enabled = false;
        statisticMenu.GetComponent<Canvas>().enabled = false;
        progressMenu.GetComponent<Canvas>().enabled = false;
        ShopMenu.GetComponent<Canvas>().enabled = false;
        ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
        ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
        PlayGame.GetComponent<Canvas>().enabled = false;
    }

    public void chengeCanvas(int mode)
    {
        mainMenu.GetComponent<Canvas>().enabled = true;
        settingMenu.GetComponent<Canvas>().enabled = false;
        choicePlayerMenu.GetComponent<Canvas>().enabled = false;
        statisticMenu.GetComponent<Canvas>().enabled = false;
        progressMenu.GetComponent<Canvas>().enabled = false;
        ShopMenu.GetComponent<Canvas>().enabled = false;
        ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
        ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
        ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
        PlayGame.GetComponent<Canvas>().enabled = false;

        if (mode == 1)
        {
            mainMenu.GetComponent<Canvas>().enabled = true;
            settingMenu.GetComponent<Canvas>().enabled = false;
            choicePlayerMenu.GetComponent<Canvas>().enabled = false;
            statisticMenu.GetComponent<Canvas>().enabled = false;
            progressMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
            PlayGame.GetComponent<Canvas>().enabled = false;
        }
        else if (mode ==2)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = true;
            choicePlayerMenu.GetComponent<Canvas>().enabled = false;
            statisticMenu.GetComponent<Canvas>().enabled = false;
            progressMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
            PlayGame.GetComponent<Canvas>().enabled = false;
        }
        else if (mode == 3)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            choicePlayerMenu.GetComponent<Canvas>().enabled = true;
            statisticMenu.GetComponent<Canvas>().enabled = false;
            progressMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
            PlayGame.GetComponent<Canvas>().enabled = false;
        }
        else if (mode == 4)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            choicePlayerMenu.GetComponent<Canvas>().enabled = false;
            statisticMenu.GetComponent<Canvas>().enabled = false;
            progressMenu.GetComponent<Canvas>().enabled = true;
            ShopMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
            PlayGame.GetComponent<Canvas>().enabled = false;
        }
        else if (mode == 5)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            choicePlayerMenu.GetComponent<Canvas>().enabled = false;
            statisticMenu.GetComponent<Canvas>().enabled = false;
            progressMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu.GetComponent<Canvas>().enabled = true;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
            PlayGame.GetComponent<Canvas>().enabled = false;
        }
        else if (mode == 6)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            choicePlayerMenu.GetComponent<Canvas>().enabled = false;
            progressMenu.GetComponent<Canvas>().enabled = false;
            statisticMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = true;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
            PlayGame.GetComponent<Canvas>().enabled = false;
        }
        else if (mode == 7)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            choicePlayerMenu.GetComponent<Canvas>().enabled = false;
            statisticMenu.GetComponent<Canvas>().enabled = false;
            progressMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = true;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
            PlayGame.GetComponent<Canvas>().enabled = false;
        }
        else if (mode == 8)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            choicePlayerMenu.GetComponent<Canvas>().enabled = false;
            statisticMenu.GetComponent<Canvas>().enabled = false;
            progressMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = true;
            PlayGame.GetComponent<Canvas>().enabled = false;
        }
        else if (mode == 9)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            choicePlayerMenu.GetComponent<Canvas>().enabled = false;
            statisticMenu.GetComponent<Canvas>().enabled = false;
            progressMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
            PlayGame.GetComponent<Canvas>().enabled = true;
        }
        else if (mode == 10)
        {
            mainMenu.GetComponent<Canvas>().enabled = false;
            settingMenu.GetComponent<Canvas>().enabled = false;
            choicePlayerMenu.GetComponent<Canvas>().enabled = false;
            statisticMenu.GetComponent<Canvas>().enabled = true;
            progressMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu.GetComponent<Canvas>().enabled = false;
            ShopMenu_Fon.GetComponent<Canvas>().enabled = false;
            ShopMenu_Sound.GetComponent<Canvas>().enabled = false;
            ShopMenu_Inteface.GetComponent<Canvas>().enabled = false;
            PlayGame.GetComponent<Canvas>().enabled = false;
        }
    }


    public void btnExit()
    {
        Application.Quit();
    }
}
