﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopMusicController : MonoBehaviour
{
    public Button buttonRight, buttonLeft, buttonPay, buttonGet;

    public Text txtInfo, txtPrice, txtNoMoney, txtCoins;
    public AudioSource audioSource;
    public AudioSource audioSourceMain;
    public AudioClip[] music;
    public AudioClip[] musicFull;
    private int key = 0;// Тип музыки
    // Start is called before the first frame update
    void Start()
    {
        audioSourceMain.GetComponent<AudioSource>().clip = musicFull[Global.keyMusic];
        audioSourceMain.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (key == 6)
        {
            buttonRight.GetComponent<Button>().interactable = false;
        }
        else
        {
            buttonRight.GetComponent<Button>().interactable = true;
        }

        if (key == 0)
        {
            buttonLeft.GetComponent<Button>().interactable = false;
            buttonPay.GetComponent<Button>().interactable = false;


        }
        else
        {
            buttonPay.GetComponent<Button>().interactable = true;
            buttonLeft.GetComponent<Button>().interactable = true;
        }
        txtCoins.GetComponent<Text>().text = CoinsSystem.coin.ToString();
    }

    public void btnRigh()
    {
        txtNoMoney.GetComponent<Text>().enabled = false;
        buttonPay.gameObject.SetActive(true);
        buttonGet.GetComponent<Button>().interactable = true;
        if (key < 6)
        {
            key++;
           
        }
        buttonGet.GetComponent<Image>().enabled = false;
        buttonGet.GetComponentInChildren<Text>().enabled = false;
         textInfoGenerate();
    }
    public void btnLeft()
    {
        txtNoMoney.GetComponent<Text>().enabled = false;
        buttonPay.gameObject.SetActive(true);
        buttonGet.GetComponent<Button>().interactable = true;
        if (key > 0)
        {
            key--;
            
        }
        buttonGet.GetComponent<Image>().enabled = false;
        buttonGet.GetComponentInChildren<Text>().enabled = false;

         textInfoGenerate();
    }

    private void textInfoGenerate()
    {
        if (key == 0)
        {
            audioSource.GetComponent<AudioSource>().clip = music[0];
            audioSource.Play();
            txtInfo.text = "Стандарт";
            txtPrice.text = "Бесплатно";
            buttonGet.GetComponent<Image>().enabled = true;
            buttonGet.GetComponentInChildren<Text>().enabled = true;
            Global.keyMusic = key;
        }
        else if (key == 1)
        {
            txtInfo.text = "SoundTrack 1";
            txtPrice.text = "200 Coins";
            audioSource.GetComponent<AudioSource>().clip = music[1];
            audioSource.Play();
            if (Global.statusMusic[key - 1] == 1)
            {
                acceptPay();
            }

        }
        else if (key == 2)
        {
            txtInfo.text = "SoundTrack 2";
            txtPrice.text = "300 Coins";
            audioSource.GetComponent<AudioSource>().clip = music[2];
            audioSource.Play();
            if (Global.statusMusic[key - 1] == 1)
            {
                acceptPay();
            }
        }
        else if (key == 3)
        {
            txtInfo.text = "SoundTrack 3";
            txtPrice.text = "300 Coins";
            audioSource.GetComponent<AudioSource>().clip = music[key];
            audioSource.Play();
            if (Global.statusMusic[key - 1] == 1)
            {
                acceptPay();
            }
        }
        else if (key == 4)
        {
            txtInfo.text = "SoundTrack 4";
            txtPrice.text = "500 Coins";
            audioSource.GetComponent<AudioSource>().clip = music[key];
            audioSource.Play();
            if (Global.statusMusic[key - 1] == 1)
            {
                acceptPay();
            }
        }
        else if (key == 5)
        {
            txtInfo.text = "SoundTrack 5";
            txtPrice.text = "500 Coins";
            audioSource.GetComponent<AudioSource>().clip = music[key];
            audioSource.Play();
            if (Global.statusMusic[key - 1] == 1)
            {
                acceptPay();
            }
        }
        else if (key == 6)
        {
            txtInfo.text = "SoundTrack 6";
            txtPrice.text = "600 Coins";
            audioSource.GetComponent<AudioSource>().clip = music[key];
            audioSource.Play();
            if (Global.statusMusic[key - 1] == 1)
            {
                acceptPay();
            }
        }
    }

    public void acceptPay()
    {
        txtNoMoney.GetComponent<Text>().text = "Приобретено";
        txtNoMoney.GetComponent<Text>().enabled = true;
        buttonGet.GetComponent<Image>().enabled = true;
        buttonGet.GetComponentInChildren<Text>().enabled = true;
    }

    public void btnPay()
    {

        if (key == 1)
        {

            if (CoinsSystem.coin >= 200)
            {
                CoinsSystem.coin = CoinsSystem.coin - 200;
                funcPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
        else if (key == 2)
        {
            if (CoinsSystem.coin >= 300)
            {
                CoinsSystem.coin = CoinsSystem.coin - 300;
                funcPay();

            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
        else if (key == 3)
        {
            if (CoinsSystem.coin >= 300)
            {
                CoinsSystem.coin = CoinsSystem.coin - 300;
                funcPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
        else if (key == 4)
        {
            if (CoinsSystem.coin >= 500)
            {
                CoinsSystem.coin = CoinsSystem.coin - 500;
                funcPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
        else if (key == 5)
        {
            if (CoinsSystem.coin >= 500)
            {
                CoinsSystem.coin = CoinsSystem.coin - 500;
                funcPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
        else if (key == 6)
        {
            if (CoinsSystem.coin >= 600)
            {
                CoinsSystem.coin = CoinsSystem.coin - 600;
                funcPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
    }
 
    public void funcPay()
    {
        txtNoMoney.GetComponent<Text>().text = "Приобретено";
        txtNoMoney.GetComponent<Text>().enabled = true;
        Global.keyMusic = key;
        Global.music = musicFull[key];
        Global.statusMusic[key - 1] = 1;
        Global.keyFirstPay = 1;
        Global.keyTwoPay += 1;
        Global.keyFullMusic += 1;
        buttonPay.gameObject.SetActive(false);
    }

    public void btnGet()
    {
        txtNoMoney.GetComponent<Text>().text = "Выбрано";
        buttonGet.GetComponent<Button>().interactable = false;
        Global.keyMusic = key;
        Global.music = musicFull[key];
        buttonPay.gameObject.SetActive(false);
    }

    public void btnBack()
    {
        Global.music = musicFull[key];
        audioSourceMain.GetComponent<AudioSource>().clip = musicFull[Global.keyMusic];
        audioSourceMain.Play();
    }
}
