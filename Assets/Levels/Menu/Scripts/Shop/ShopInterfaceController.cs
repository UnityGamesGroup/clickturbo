﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopInterfaceController : MonoBehaviour
{
    public Button buttonRight, buttonLeft, buttonPay, buttonGet;

    public Text txtInfo, txtPrice, txtNoMoney, txtCoins;
    public Sprite[] imgBack, imgInfo, imgStart, imgLeft, imgRight, imgClose, imgProgress, imgShop, imgSetting, imgSound, imgStatistic;
    private int key = 0;// Тип музыки
    // Start is called before the first frame update
    void Start()
    {
        key = Global.keyIcon;
        interfaceEngine();
        key = 0;
    }

    // Update is called once per frame
    void Update()
    {
        textInfoGenerate();
        if (key == 4)
        {
            buttonRight.GetComponent<Button>().interactable = false;
        }
        else
        {
            buttonRight.GetComponent<Button>().interactable = true;
        }

        if (key == 0)
        {
            buttonLeft.GetComponent<Button>().interactable = false;
            buttonPay.GetComponent<Button>().interactable = false;

        }
        else
        {
            buttonPay.GetComponent<Button>().interactable = true;
            buttonLeft.GetComponent<Button>().interactable = true;
        }
        txtCoins.GetComponent<Text>().text = CoinsSystem.coin.ToString();
    }

    public void btnRigh()
    {
        txtNoMoney.GetComponent<Text>().enabled = false;
        buttonPay.gameObject.SetActive(true);
        buttonGet.GetComponent<Button>().interactable = true;
        if (key < 4)
        {
            key++;
            interfaceEngineTest();
        }
        buttonGet.GetComponent<Image>().enabled = false;
        buttonGet.GetComponentInChildren<Text>().enabled = false;
        textInfoGenerate();
    }
    public void btnLeft()
    {
        txtNoMoney.GetComponent<Text>().enabled = false;
        buttonPay.gameObject.SetActive(true);
        buttonGet.GetComponent<Button>().interactable = true;
        if (key > 0)
        {
            key--;
            interfaceEngineTest();
        }
        buttonGet.GetComponent<Image>().enabled = false;
        buttonGet.GetComponentInChildren<Text>().enabled = false;
        textInfoGenerate();
    }

    private void textInfoGenerate()
    {
        if (key == 0)
        {
            txtInfo.text = "Стандарт";
            txtPrice.text = "Бесплатно";
            buttonGet.GetComponent<Image>().enabled = true;
            buttonGet.GetComponentInChildren<Text>().enabled = true;
        }
        else if (key == 1)
        {
            txtInfo.text = "Стиль 1";
            txtPrice.text = "400 Coins";
            if (Global.statusIcon[key - 1] == 1)
            {
                acceptPay();
            }

        }
        else if (key == 2)
        {
            txtInfo.text = "Стиль 2";
            txtPrice.text = "200 Coins";
            if (Global.statusIcon[key - 1] == 1)
            {
                acceptPay();
            }
        }
        else if (key == 3)
        {
            txtInfo.text = "Стиль 3";
            txtPrice.text = "100 Coins";
            if (Global.statusIcon[key - 1] == 1)
            {
                acceptPay();
            }
        }
        else if (key == 4)
        {
            txtInfo.text = "Стиль 4";
            txtPrice.text = "300 Coins";
            if (Global.statusIcon[key - 1] == 1)
            {
                acceptPay();
            }
        }
    }

    public void acceptPay()
    {
        txtNoMoney.GetComponent<Text>().text = "Приобретено";
        txtNoMoney.GetComponent<Text>().enabled = true;
        buttonGet.GetComponent<Image>().enabled = true;
        buttonGet.GetComponentInChildren<Text>().enabled = true;
    }

    public void btnPay()
    {

        if (key == 1)
        {

            if (CoinsSystem.coin >= 400)
            {
                CoinsSystem.coin = CoinsSystem.coin - 400;
                funcPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
        else if (key == 2)
        {
            if (CoinsSystem.coin >= 300)
            {
                CoinsSystem.coin = CoinsSystem.coin - 200;
                funcPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
        else if (key == 3)
        {
            if (CoinsSystem.coin >= 150)
            {
                CoinsSystem.coin = CoinsSystem.coin - 100;
                funcPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
        else if (key == 4)
        {
            if (CoinsSystem.coin >= 300)
            {
                CoinsSystem.coin = CoinsSystem.coin - 300;
                funcPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
    }

    public void funcPay()
    {
        txtNoMoney.GetComponent<Text>().text = "Приобретено";
        txtNoMoney.GetComponent<Text>().enabled = true;
        Global.statusIcon[key - 1] = 1;
        interfaceEngine();
        Global.keyIcon = key;
        Global.keyFirstPay = 1;
        Global.keyTwoPay += 1;
        Global.keyFullInterface += 1;
        buttonPay.gameObject.SetActive(false);
    }

    public void btnGet()
    {
        txtNoMoney.GetComponent<Text>().text = "Выбрано";
        buttonGet.GetComponent<Button>().interactable = false;
        buttonPay.gameObject.SetActive(false);
        Global.keyIcon = key;
        interfaceEngine();
    }

    private void interfaceEngine()
    {
        var mas = GameObject.FindGameObjectsWithTag("btnBack");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgBack[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnInfo");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgInfo[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnStart");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgStart[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnLeft");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgLeft[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnRight");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgRight[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnClose");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgClose[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnProgress");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgProgress[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnShop");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgShop[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnSetting");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgSetting[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnSound");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgSound[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnStatistic");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgStatistic[key];
        }
    }

    private void interfaceEngineTest()//Пример
    {
        var mas = GameObject.FindGameObjectsWithTag("btnBackTest");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgBack[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnStartTest");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgStart[key];
        }
        mas = GameObject.FindGameObjectsWithTag("btnLeftTest");
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i].GetComponent<Image>().sprite = imgLeft[key];
        }
    }

}
