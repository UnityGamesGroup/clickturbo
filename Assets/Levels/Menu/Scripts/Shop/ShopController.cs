﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class ShopController : MonoBehaviour
{
    public GameObject SwipePanel;
    public Button buttonRight, buttonLeft, buttonPay, buttonGet;
    public Text txtInfo, txtPrice, txtNoMoney, txtCoins;
    public GameObject pnlFon;
    public Sprite[] fon;
    private int key = 0;// Тип фона

    // Start is called before the first frame update
    void Start()
    {
        pnlFon.GetComponent<Image>().sprite = fon[key];
    }

    // Update is called once per frame
    void Update()
    {
        if (key == 11)
        {
            buttonRight.GetComponent<Button>().interactable = false;
        }
        else
        {
            buttonRight.GetComponent<Button>().interactable = true;
        }

        if (key == 0)
        {
            buttonLeft.GetComponent<Button>().interactable = false;
            buttonPay.GetComponent<Button>().interactable = false;
            
        }
        else
        {
            buttonPay.GetComponent<Button>().interactable = true;
            buttonLeft.GetComponent<Button>().interactable = true;
        }
        Global.globalFon = fon[Global.keyFon];
        txtCoins.GetComponent<Text>().text = CoinsSystem.coin.ToString();
    }

    public void btnRigh()
    {
        txtNoMoney.GetComponent<Text>().enabled = false;
        buttonPay.gameObject.SetActive(true);
        buttonGet.GetComponent<Button>().interactable = true;
        if (key < 11)
        {
            key++;
            pnlFon.GetComponent<Image>().sprite = fon[key];
        }
        buttonGet.GetComponent<Image>().enabled = false;
        buttonGet.GetComponentInChildren<Text>().enabled = false;
        textInfoGenerate();
    }
    public void btnLeft()
    {
        txtNoMoney.GetComponent<Text>().enabled = false;
        buttonPay.gameObject.SetActive(true);
        buttonGet.GetComponent<Button>().interactable = true;
        if (key > 0)
        {
            key--;
            pnlFon.GetComponent<Image>().sprite = fon[key];
        }
        buttonGet.GetComponent<Image>().enabled = false;
        buttonGet.GetComponentInChildren<Text>().enabled = false;
        textInfoGenerate();
    }

    private void textInfoGenerate()
    {
        if (key == 0)
        {
            txtInfo.text = "Стандарт";
            txtPrice.text = "Бесплатно";
            buttonGet.GetComponent<Image>().enabled = true;
            buttonGet.GetComponentInChildren<Text>().enabled = true;
        }
        else if (key == 1)
        {
            txtInfo.text = "Техно 1";
            txtPrice.text = "20 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }

        }
        else if (key == 2)
        {
            txtInfo.text = "Техно 2";
            txtPrice.text = "30 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
        }
        else if (key == 3)
        {
            txtInfo.text = "Техно 3";
            txtPrice.text = "60 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
        }
        else if (key == 4)
        {
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
            txtInfo.text = "Техно 4";
            txtPrice.text = "80 Coins";
        }
        else if (key == 5)
        {
            txtInfo.text = "Sci-Fi 1";
            txtPrice.text = "100 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
        }
        else if (key == 6)
        {
            txtInfo.text = "Sci-Fi 2";
            txtPrice.text = "150 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
        }
        else if (key == 7)
        {
            txtInfo.text = "Sci-Fi 3";
            txtPrice.text = "300 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
        }
        else if (key == 8)
        {
            txtInfo.text = "Аниме 1";
            txtPrice.text = "400 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
        }
        else if (key == 9)
        {
            txtInfo.text = "Аниме 2";
            txtPrice.text = "800 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
        }
        else if (key == 10)
        {
            txtInfo.text = "Аниме 3";
            txtPrice.text = "900 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
        }
        else if (key == 11)
        {
            txtInfo.text = "Аниме 4";
            txtPrice.text = "1000 Coins";
            if (Global.fon[key - 1] == 1)
            {
                funcStatus();
            }
        }

    }

    public void funcStatus()
    {
        txtNoMoney.GetComponent<Text>().text = "Приобретено";
        txtNoMoney.GetComponent<Text>().enabled = true;
        buttonGet.GetComponent<Image>().enabled = true;
        buttonGet.GetComponentInChildren<Text>().enabled = true;
    }

    public void btnPay()
    {

        if (key == 1)
        {

            if (CoinsSystem.coin >= 20)
            {
                CoinsSystem.coin = CoinsSystem.coin - 20;  
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
        else if (key == 2)
        {
            if (CoinsSystem.coin >= 30)
            {
                CoinsSystem.coin = CoinsSystem.coin - 30;
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }

        }
        else if (key == 3)
        {
            if (CoinsSystem.coin >= 60)
            {
                CoinsSystem.coin = CoinsSystem.coin - 60;
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
        else if (key == 4)
        {
            if (CoinsSystem.coin >= 80)
            {
                CoinsSystem.coin = CoinsSystem.coin - 80;
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
        else if (key == 5)
        {
            if (CoinsSystem.coin >= 100)
            {
                CoinsSystem.coin = CoinsSystem.coin - 100;
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
        else if (key == 6)
        {
            if (CoinsSystem.coin >= 150)
            {
                CoinsSystem.coin = CoinsSystem.coin - 150;
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
        else if (key == 7)
        {
            if (CoinsSystem.coin >= 300)
            {
                CoinsSystem.coin = CoinsSystem.coin - 300;
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
        else if (key == 8)
        {
            if (CoinsSystem.coin >= 400)
            {
                CoinsSystem.coin = CoinsSystem.coin - 400;
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
        else if (key == 9)
        {
            if (CoinsSystem.coin >= 800)
            {
                CoinsSystem.coin = CoinsSystem.coin - 800;
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
        else if (key == 10)
        {
            if (CoinsSystem.coin >= 900)
            {
                CoinsSystem.coin = CoinsSystem.coin - 900; 
                acceptPay();

            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
        else if (key == 11)
        {
            if (CoinsSystem.coin >= 1000)
            {
                CoinsSystem.coin = CoinsSystem.coin - 1000;
                acceptPay();
            }
            else
            {
                txtNoMoney.GetComponent<Text>().text = "Недостаточно средств";
                txtNoMoney.GetComponent<Text>().enabled = true;
            }
        }
    }

    public void acceptPay() 
    {
        txtNoMoney.GetComponent<Text>().text = "Приобретено";
        txtNoMoney.GetComponent<Text>().enabled = true;
        Global.fon[key - 1] = 1;
        Global.globalFon = fon[key];
        Global.keyFon = key;
        Global.keyFirstPay = 1;
        Global.keyTwoPay += 1;
        Global.keyFullFon += 1;
        buttonPay.gameObject.SetActive(false);
    }

    public void btnGet()
    {
        Global.keyFon = key;
        txtNoMoney.GetComponent<Text>().text = "Выбрано";
        buttonGet.GetComponent<Button>().interactable = false;
        buttonPay.gameObject.SetActive(false);
        Global.globalFon = fon[key];
    }

}
