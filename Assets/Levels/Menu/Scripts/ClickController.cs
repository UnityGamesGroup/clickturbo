﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class ClickController : MonoBehaviour
{
    
    public Sprite[] clickColor;
    public Sprite[] clickCenterColor;

    public Button btn;
    public Image spriteMain;
    public Text countCoints;
    public GameObject pnlInfo, pnlProgress;

    private int value;
    public AudioSource clip;

    private const string leaderBoard = "CgkI9bX4jdkTEAIQAA";

    private const string progress1 = "CgkI9bX4jdkTEAIQAg";
    private const string progress2 = "CgkI9bX4jdkTEAIQAw";
    private const string progress3 = "CgkI9bX4jdkTEAIQBA";
    private const string progress4 = "CgkI9bX4jdkTEAIQBQ";
    private const string progress5 = "CgkI9bX4jdkTEAIQBg";
    private const string progress6 = "CgkI9bX4jdkTEAIQBw";
    private const string progress7 = "CgkI9bX4jdkTEAIQCA";
    private const string progress8 = "CgkI9bX4jdkTEAIQCQ";
    private const string progress9 = "CgkI9bX4jdkTEAIQCg";
    private const string progress10 = "CgkI9bX4jdkTEAIQCw";
    private const string progress11 = "CgkI9bX4jdkTEAIQDA";
    private const string progress12 = "CgkI9bX4jdkTEAIQDQ";
    private const string progress13 = "CgkI9bX4jdkTEAIQDg";
    private const string progress14 = "CgkI9bX4jdkTEAIQDw";
    private const string progress15 = "CgkI9bX4jdkTEAIQEA";
    // Start is called before the first frame update
    void Start()
    {
        pnlInfo.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (value>=100 && Global.progress[0]!=1)
        {
            GetTheProgress(progress1);
            Global.progress[0] = 1;
            playAnimProgress();
        }
        if (value >= 200 && Global.progress[1] != 1)
        {
            GetTheProgress(progress2);
            Global.progress[1] = 1;
            playAnimProgress();
        }
        if (value >= 500 && Global.progress[2] != 1)
        {
            GetTheProgress(progress3);
            Global.progress[2] = 1;
            playAnimProgress();
        }
        if (value >= 1000 && Global.progress[3] != 1)
        {
            GetTheProgress(progress4);
            Global.progress[3] = 1;
            playAnimProgress();
        }
        if (CoinsSystem.coin >= 100 && Global.progress[4] != 1)
        {
            GetTheProgress(progress5);
            Global.progress[4] = 1;
            playAnimProgress();
        }
        if (CoinsSystem.coin >= 200 && Global.progress[5] != 1)
        {
            GetTheProgress(progress6);
            Global.progress[5] = 1;
            playAnimProgress();
        }
        if (CoinsSystem.coin >= 500 && Global.progress[6] != 1)
        {
            GetTheProgress(progress7);
            Global.progress[6] = 1;
            playAnimProgress();
        }
        if (CoinsSystem.coin >= 1000 && Global.progress[7] != 1)
        {
            GetTheProgress(progress8);
            Global.progress[7] = 1;
            playAnimProgress();
        }
        if (Global.keyFirstPay == 1 && Global.progress[8] != 1)
        {
            GetTheProgress(progress9);
            Global.progress[8] = 1;
            playAnimProgress();
        }
        if (Global.keyTwoPay > 1 && Global.progress[9] != 1)
        {
            GetTheProgress(progress10);
            Global.progress[9] = 1;
            playAnimProgress();
        }
        if (Global.keyFullFon == 11 && Global.progress[10] != 1)
        {
            GetTheProgress(progress11);
            Global.progress[10] = 1;
            playAnimProgress();
        }
        if (Global.keyFullMusic == 6 && Global.progress[11] != 1)
        {
            GetTheProgress(progress12);
            Global.progress[11] = 1;
            playAnimProgress();
        }
        if (Global.keyFullInterface == 3 && Global.progress[12] != 1)
        {
            GetTheProgress(progress13);
            Global.progress[12] = 1;
            playAnimProgress();
        }
        if (Global.keyFullInterface == 3 && Global.keyFullFon == 11 && Global.keyFullMusic == 6 && Global.progress[13] != 1)
        {
            GetTheProgress(progress14);
            Global.progress[13] = 1;
            playAnimProgress();
        }
        if (value >= 10000 && Global.progress[14] != 1)
        {
            GetTheProgress(progress15);
            Global.progress[14] = 1;
            playAnimProgress();
        }
    }
    
    public void btnClick()
    {
        
        if (Input.touchCount < 2)
        {
            btn.GetComponent<Animator>().enabled = true;
            btn.GetComponent<Animator>().Play("clickAnim", -1, 0f);
            clip.GetComponent<AudioSource>().Play();
            value++;
            btn.GetComponent<Image>().sprite = clickColor[Random.Range(0, clickColor.Length)];
            spriteMain.GetComponent<Image>().sprite = clickCenterColor[Random.Range(0, clickCenterColor.Length)];

            if (CoinsSystem.mode == 2)
            {
                value = value + 1;
            }
            else if (CoinsSystem.mode == 3)
            {
                value = value + 2;
            }
            else if (CoinsSystem.mode == 4)
            {
                value = value + 3;
            }

            countCoints.GetComponent<Text>().text = value.ToString();
            Global.valueClick++;
        }
        Social.ReportScore(Global.valueClick, leaderBoard, (bool success) => { });

    }

    public void showLeaderBoard()
    {
        Social.ShowLeaderboardUI();
    }


    public void GetTheProgress(string id)
    {
        Social.ReportProgress(id, 100.0f, (bool success) =>
            {
                if (success)
                {
                    print("Получено достижение: " + id);
                }
            });
    }

    public void ExitFromGPS()
    {
        PlayGamesPlatform.Instance.SignOut();
    }

    public void btnExit()
    {
        Global.valuePoints = Global.valuePoints + value;
        if (value>= 10)
        {
            value = value / 10;
        }
        else
        {
            value = 0;
        }
        Global.valueCoins = Global.valueCoins + value;
        CoinsSystem.coin = CoinsSystem.coin + value;
        value = 0;
        countCoints.GetComponent<Text>().text = value.ToString();
    }

    public void btnClose()
    {
        pnlInfo.SetActive(false);
    }

    public void btnInfo()
    {
        pnlInfo.SetActive(true);
    }
    public void playAnimProgress()
    {
        pnlProgress.gameObject.SetActive(true);
        pnlProgress.GetComponent<Animator>().enabled = true;
        pnlProgress.GetComponent<Animator>().Play("progressAnim", -1, 0f);
    }
}
