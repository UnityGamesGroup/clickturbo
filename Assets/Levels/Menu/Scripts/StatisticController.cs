﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatisticController : MonoBehaviour
{
    public Text valueClick;
    public Text valuePoins;
    public Text valueCoins;
    // Start is called before the first frame update
    void Start()
    {
        valueClick.text = Global.valueClick.ToString();
        valueCoins.text = Global.valueCoins.ToString();
        valuePoins.text = Global.valuePoints.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        valueClick.text = Global.valueClick.ToString();
        valueCoins.text = Global.valueCoins.ToString();
        valuePoins.text = Global.valuePoints.ToString();
    }
}
