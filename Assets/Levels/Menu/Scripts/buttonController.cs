﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class buttonController : MonoBehaviour
{
    public Image x2, x3, x4;
    public int coins;
    public Image[] okInfo;
    public Text coinsCount;
    public GameObject lockx2, lockx3, lockx4;

    public AudioSource audioControl, audioDemoControl;
    public Slider slider;

    void Start()
    {
        coins = CoinsSystem.coin;
        CoinsStart();
        slider.value = Global.volume;
        okInfo[0].gameObject.SetActive(false);
        okInfo[1].gameObject.SetActive(false);
        okInfo[2].gameObject.SetActive(false);
    }
    void Update()
    {
        CoinsStart();
        coinsCount.GetComponent<Text>().text = coins.ToString();
        if (CoinsSystem.coin >= 50 && CoinsSystem.coin < 150)
        {
            lockx2.SetActive(false);
            lockx3.SetActive(true);
            lockx4.SetActive(true);
            x2.GetComponent<Button>().enabled = true;
            x3.GetComponent<Button>().enabled = false;
            x4.GetComponent<Button>().enabled = false;
        }
        else if (CoinsSystem.coin >= 150 && CoinsSystem.coin < 400)
        {
            lockx2.SetActive(false);
            lockx3.SetActive(false);
            lockx4.SetActive(true);
            x2.GetComponent<Button>().enabled = true;
            x3.GetComponent<Button>().enabled = true;
            x4.GetComponent<Button>().enabled = false;
        }
        else if (CoinsSystem.coin >= 400)
        {
            lockx2.SetActive(false);
            lockx3.SetActive(false);
            lockx4.SetActive(false);
            x2.GetComponent<Button>().enabled = true;
            x3.GetComponent<Button>().enabled = true;
            x4.GetComponent<Button>().enabled = true;
        }
        Global.volume = audioControl.volume;
        audioDemoControl.volume = Global.volume;
    }

    public void CoinsStart()
    {
        if (CoinsSystem.coin < 50)
        {
            lockx2.SetActive(true);
            lockx3.SetActive(true);
            lockx4.SetActive(true);
            x2.GetComponent<Button>().enabled = false;
            x3.GetComponent<Button>().enabled = false;
            x4.GetComponent<Button>().enabled = false;
            
        }
        
    }

    public void btnboostX2()
    {
        coins = CoinsSystem.coin - 50;
        CoinsSystem.mode = 2;
        x2.GetComponent<Button>().interactable = false;
        x3.GetComponent<Button>().interactable = true;
        x4.GetComponent<Button>().interactable = true;
        okInfo[0].gameObject.SetActive(true);
        okInfo[1].gameObject.SetActive(false);
        okInfo[2].gameObject.SetActive(false);
    }

    public void btnboostX3()
    {
        coins = CoinsSystem.coin - 150;
        CoinsSystem.mode = 3;
        x2.GetComponent<Button>().interactable = true;
        x4.GetComponent<Button>().interactable = true;
        x3.GetComponent<Button>().interactable = false;
        okInfo[0].gameObject.SetActive(false);
        okInfo[1].gameObject.SetActive(true);
        okInfo[2].gameObject.SetActive(false);
    }

    public void btnboostX4()
    {
        coins = CoinsSystem.coin - 400;
        CoinsSystem.mode = 4;
        x3.GetComponent<Button>().interactable = true;
        x2.GetComponent<Button>().interactable = true;
        x4.GetComponent<Button>().interactable = false;
        okInfo[0].gameObject.SetActive(false);
        okInfo[1].gameObject.SetActive(false);
        okInfo[2].gameObject.SetActive(true);
    }

    public void btnPlay()
    {
        CoinsSystem.coin = coins;     
    }
    public void btnBack()
    {
        okInfo[0].gameObject.SetActive(false);
        okInfo[1].gameObject.SetActive(false);
        okInfo[2].gameObject.SetActive(false);
        x3.GetComponent<Button>().interactable = true;
        x2.GetComponent<Button>().interactable = true;
        x4.GetComponent<Button>().interactable = true;
        CoinsSystem.mode = 1;
        coins = CoinsSystem.coin;
    }
    public void btnStart()
    {
        coins = CoinsSystem.coin;
        okInfo[0].gameObject.SetActive(false);
        okInfo[1].gameObject.SetActive(false);
        okInfo[2].gameObject.SetActive(false);
        x3.GetComponent<Button>().interactable = true;
        x2.GetComponent<Button>().interactable = true;
        x4.GetComponent<Button>().interactable = true;
        CoinsSystem.mode = 1;
    }
}
