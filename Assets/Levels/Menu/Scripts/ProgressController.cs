﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressController : MonoBehaviour
{
    public GameObject[] lockPanel;


    // Start is called before the first frame update
    void Start()
    {
 
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < Global.progress.Length; i++)
        {
            if (Global.progress[i] == 1)
            {
                lockPanel[i].SetActive(false);
            }
        } 
     
    }
}
