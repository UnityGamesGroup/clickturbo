﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundController : MonoBehaviour
{
    public GameObject[] fon;
    public Sprite standartFon;
    // Start is called before the first frame update
    void Start()
    {
        if (Global.keyFon == 0)
        {
            for (int i = 0; i < fon.Length; i++)
            {
                fon[i].GetComponent<Image>().sprite = standartFon;
            }
        }
        else
        {
            for (int i = 0; i < fon.Length; i++)
            {
                fon[i].GetComponent<Image>().sprite = Global.globalFon;
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Global.keyFon == 0)
        {
            for (int i = 0; i < fon.Length; i++)
            {
                fon[i].GetComponent<Image>().sprite = standartFon;
            }
        }
        else
        {
            for (int i = 0; i < fon.Length; i++)
            {
                fon[i].GetComponent<Image>().sprite = Global.globalFon;
            }
        }
    }
}
