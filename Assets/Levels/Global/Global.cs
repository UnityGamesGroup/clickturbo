﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour
{
    public static Sprite globalFon;
    public static AudioClip music;
    public static int valuePoints,valueCoins,valueClick; // Для статистики

    public static Sprite globalIconBack, globalIconInfo, globalIconStart;
    public static int keyMusic, keyFon, keyIcon;
    public static int keyFirstPay, keyTwoPay, keyFullFon, keyFullMusic, keyFullInterface;//ключи прогресса
    public static string masProgress, masFon, masMusic, masIcon;//массивы для сохранения статуса
    public static float volume = 0.5f;

    public static int num;
    public static int[] progress = new int[14];//ключ получено или неполучино достижение
    public static int[] fon = new int[14]; //ключ куплен или некуплен для фона
    public static int[] statusMusic = new int[10]; //ключ куплен или некуплен для звука
    public static int[] statusIcon = new int[10];//ключ куплен или некуплен для интерфейса

}
