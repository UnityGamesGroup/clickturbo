﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SaveGame : MonoBehaviour
{
    // Start is called before the first frame update

    void Awake()
    {
        //PlayerPrefs.DeleteAll();
        LoadData();

        if (PlayerPrefs.HasKey("masProgress"))
        {
            string[] progress = Global.masProgress.Split(',');
            for (int i = 0; i < Global.progress.Length; i++)
            {
                Global.num = Convert.ToInt32(progress[i]);
                Global.progress[i] = Global.num;
            }
        }
        if (PlayerPrefs.HasKey("masFon"))
        {
            string[] fon = Global.masFon.Split(',');
            for (int i = 0; i < Global.fon.Length; i++)
            {
                Global.num = Convert.ToInt32(fon[i]);
                Global.fon[i] = Global.num;
            }
        }
        if (PlayerPrefs.HasKey("masMusic"))
        {
            string[] music = Global.masMusic.Split(',');
            for (int i = 0; i < Global.statusMusic.Length; i++)
            {
                Global.num = Convert.ToInt32(music[i]);
                Global.statusMusic[i] = Global.num;
            }
        }
        if (PlayerPrefs.HasKey("masIcon"))
        {
            string[] icon = Global.masIcon.Split(',');
            for (int i = 0; i < Global.statusIcon.Length; i++)
            {
                Global.num = Convert.ToInt32(icon[i]);
                Global.statusIcon[i] = Global.num;
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for (int i = 0; i < Global.progress.Length; i++)
        {
            Global.masProgress += Global.progress[i].ToString() + ",";
        }
        for (int i = 0; i < Global.fon.Length; i++)
        {
            Global.masFon += Global.fon[i].ToString() + ",";
        }
        for (int i = 0; i < Global.statusMusic.Length; i++)
        {
            Global.masMusic += Global.statusMusic[i].ToString() + ",";
        }
        for (int i = 0; i < Global.statusIcon.Length; i++)
        {
            Global.masIcon += Global.statusIcon[i].ToString() + ",";
        }
        SaveData();
        Global.masProgress = null;
        Global.masFon = null;
        Global.masMusic = null;
        Global.masIcon = null;
    }

    void SaveData()
    {
        PlayerPrefs.SetInt("valueClick", Global.valueClick);
        PlayerPrefs.SetInt("valueCoins", Global.valueCoins);
        PlayerPrefs.SetInt("valuePoints", Global.valuePoints);

        PlayerPrefs.SetFloat("volume", Global.volume);
        PlayerPrefs.SetInt("SavedCoins", CoinsSystem.coin);
        PlayerPrefs.SetInt("keyFon", Global.keyFon);
        PlayerPrefs.SetInt("keyMusic", Global.keyMusic);
        PlayerPrefs.SetInt("keyIcon", Global.keyIcon);

        PlayerPrefs.SetString("masProgress", Global.masProgress);
        PlayerPrefs.SetString("masFon", Global.masFon);
        PlayerPrefs.SetString("masMusic", Global.masMusic);
        PlayerPrefs.SetString("masIcon", Global.masIcon);

        PlayerPrefs.SetInt("keyFirstPay", Global.keyFirstPay);
        PlayerPrefs.SetInt("keyTwoPay", Global.keyTwoPay);
        PlayerPrefs.SetInt("keyFullFon", Global.keyFullFon);
        PlayerPrefs.SetInt("keyFullMusic", Global.keyFullMusic);
        PlayerPrefs.SetInt("keyFullInterface", Global.keyFullInterface);

        PlayerPrefs.Save();
    }
    
    void LoadData()
    {
        if (PlayerPrefs.HasKey("valueClick"))
        {
            Global.valueClick = PlayerPrefs.GetInt("valueClick"); 
        }
        if (PlayerPrefs.HasKey("valueCoins"))
        {
            Global.valueCoins = PlayerPrefs.GetInt("valueCoins");
        }
        if (PlayerPrefs.HasKey("valuePoints"))
        {
            Global.valuePoints = PlayerPrefs.GetInt("valuePoints");
        }
        if (PlayerPrefs.HasKey("volume"))
        {
            Global.volume = PlayerPrefs.GetFloat("volume");
        }
        if (PlayerPrefs.HasKey("SavedCoins"))
        {
            CoinsSystem.coin = PlayerPrefs.GetInt("SavedCoins");
        }
        if (PlayerPrefs.HasKey("masProgress"))
        {
            Global.masProgress = PlayerPrefs.GetString("masProgress");
        }
        if (PlayerPrefs.HasKey("masFon"))
        {
            Global.masFon = PlayerPrefs.GetString("masFon");
        }
        if (PlayerPrefs.HasKey("masMusic"))
        {
            Global.masMusic = PlayerPrefs.GetString("masMusic");
        }
        if (PlayerPrefs.HasKey("masIcon"))
        {
            Global.masIcon = PlayerPrefs.GetString("masIcon");
        }
        if (PlayerPrefs.HasKey("keyFon"))
        {
            Global.keyFon = PlayerPrefs.GetInt("keyFon");
        }
        if (PlayerPrefs.HasKey("keyMusic"))
        {
            Global.keyMusic = PlayerPrefs.GetInt("keyMusic");
        }
        if (PlayerPrefs.HasKey("keyIcon"))
        {
            Global.keyIcon = PlayerPrefs.GetInt("keyIcon");
        }
        if (PlayerPrefs.HasKey("keyFirstPay"))
        {
            Global.keyFirstPay = PlayerPrefs.GetInt("keyFirstPay");
        }
        if (PlayerPrefs.HasKey("keyTwoPay"))
        {
            Global.keyTwoPay = PlayerPrefs.GetInt("keyTwoPay");
        }
        if (PlayerPrefs.HasKey("keyFullFon"))
        {
            Global.keyFullFon = PlayerPrefs.GetInt("keyFullFon");
        }
        if (PlayerPrefs.HasKey("keyFullMusic"))
        {
            Global.keyFullMusic = PlayerPrefs.GetInt("keyFullMusic");
        }
        if (PlayerPrefs.HasKey("keyFullInterface"))
        {
            Global.keyFullInterface = PlayerPrefs.GetInt("keyFullInterface");
        }
    }
    
}
